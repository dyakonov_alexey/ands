#include <iostream>
#include "avltree.h"

AVL :: AVL()
{
    this -> root = nullptr;
}

AVL :: ~AVL()
{
    MakeEmpty(this -> root);
}

void AVL :: MakeEmpty(Node *t)
{
    if (t == nullptr)
        return;
    MakeEmpty(t -> Left);
    MakeEmpty(t -> Right);
    delete t;
}


Node* AVL :: Insert(int x, Node *t)
{
    if (t == nullptr)
    {
        t = new Node;
        t -> Data = x;
        t -> Height = 0;
        t -> Left = t -> Right = nullptr;
    }

    else if (x < t -> Data)
    {
        t -> Left = Insert(x, t -> Left);
        if (height(t -> Left) - height(t -> Right) == 2)
        {
            if (x < t -> Left -> Data)
                t = SingleLeftRotate(t);
            else
                t = DoubleLeftRotate(t);
        }
    }

    else if (x > t -> Data)
    {
        t -> Right = Insert(x, t -> Right);
        if (height(t -> Right) - height(t -> Left) == 2)
        {
            if (x > t -> Right -> Data)
                t = SingleRightRotate(t);
            else
                t = DoubleRightRotate(t);
        }
    }

    t -> Height = std :: max(height(t -> Left), height(t -> Right)) + 1;
    return t;
}


Node* AVL :: SingleLeftRotate(Node *&t)
{
    Node *u = t -> Left;

    t -> Left = u -> Right;
    u -> Right = t;

    t -> Height = std :: max(height(t -> Left), height(t -> Right)) + 1;
    u -> Height = std :: max(height(u -> Left), height(u -> Right)) + 1;

    return u;
}


Node* AVL :: SingleRightRotate(Node *&t)
{
    Node *u = t -> Right;

    t -> Right = u -> Left;
    u -> Left = t;

    t -> Height = std :: max(height(t -> Left), height(t -> Right)) + 1;
    u -> Height = std :: max(height(u -> Left), height(u -> Right)) + 1;

    return u;
}


Node* AVL :: DoubleRightRotate(Node *&t)
{
    t -> Right = SingleLeftRotate(t -> Right);
    return SingleRightRotate(t);
}


Node* AVL :: DoubleLeftRotate(Node *&t)
{
    t -> Left = SingleRightRotate(t -> Left);
    return SingleLeftRotate(t);
}


Node* AVL :: FindMin(Node *t)
{
    if(t == nullptr)
        return nullptr;
    else if (t -> Left == nullptr)
        return t;
    else
        return FindMin(t -> Left);
}


Node* AVL :: FindMax(Node *t)
{
    if(t == nullptr)
        return nullptr;
    else if (t -> Right == nullptr)
        return t;
    else
        return FindMax(t -> Right);
}


Node* AVL :: Remove(int x, Node *t)
{
    Node *tmp;

    if (t == nullptr)
        return nullptr;

    else if (x < t -> Data)
        t -> Left = Remove(x, t -> Left);

    else if (x > t -> Data)
        t -> Right = Remove(x, t -> Right);

    else if (t -> Left && t -> Right)
    {
        tmp = FindMin(t -> Right);
        t -> Data = tmp -> Data;
        t -> Right = Remove(t -> Data, t -> Right);
    }

    else
    {
        tmp = t;
        if (t -> Left == nullptr)
            t = t -> Right;
        else if (t -> Right == nullptr)
            t = t -> Left;
        delete tmp;
    }

    if (t == nullptr)
        return t;

    t -> Height = std :: max(height(t -> Left), height(t -> Right)) + 1;

    if (height(t -> Left) - height(t -> Right) == 2)
    {
        if (height( t -> Left -> Left) - height(t -> Left -> Right) == 1)
            return SingleLeftRotate(t);
        else
            return DoubleLeftRotate(t);
    }

    else if (height(t -> Right) - height(t -> Left) == 2)
    {
        if (height(t -> Right -> Right) - height(t -> Right -> Left) == 1)
            return SingleRightRotate(t);
        else
            return DoubleRightRotate(t);
    }

    return t;
}


int AVL :: height(Node *t)
{
    return (t == nullptr ? -1 : t -> Height);
}


int AVL :: GetBalance(Node *t)
{
    if (t == nullptr)
        return 0;
    else
        return height(t -> Left) - height(t -> Right);
}


void AVL :: InOrder(Node *t)
{
    if (t == nullptr)
        return;
    InOrder(t -> Left);
    std :: cout << t -> Data << " ";
    InOrder(t -> Right);
}


void AVL :: Insert(int x)
{
    this -> root = Insert(x, this -> root);
}


void AVL :: Remove(int x)
{
    this -> root = Remove(x, this -> root);
}


void AVL :: Display()
{
    InOrder(this -> root);
    std :: cout << std ::  endl;
}

