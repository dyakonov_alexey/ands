#include "rpn.h"

int Precedence(const char &c)
{
    if(c=='(')
        return(0);
    if(c=='+'||c=='-')
        return(1);
    if(c=='*'||c=='/'||c=='%')
        return(2);

    return(3);
}

string RPN(const string &expr)
{
    const int SIZE = expr.length();

    Stack stk1(SIZE / 2);
    char x, token;
    int i, j;
    j = 0;
    string result;

    for (i = 0; expr[i] != '\0'; ++i)
    {
        token = expr[i];
        if (isalnum(token))
        {
            result.insert(j++, 1, token);
        }
        else if (token == '(')
            stk1.Push(token);
        else if (token == ')')
            while(stk1.Top() != '(')
            {
                x = stk1.Top();
                stk1.Pop();
                result.insert(j++, 1, x);
            }
        else
        {
            while(!stk1.IsEmpty() && Precedence(token) <= Precedence(stk1.Top()))
            {
                x = stk1.Top();
                stk1.Pop();
                result.insert(j++, 1, x);
            }
            stk1.Push(token);
        }
    }

    while(!stk1.IsEmpty())
    {
        x = stk1.Top();
        stk1.Pop();
        if (x == '(')
            continue;
        result.insert(j++, 1, x);
    }

    return result;
}
