#pragma once

class Stack {
    char *arr;
    unsigned Capacity;
    unsigned CurrentSize;

public:
//    Stack();
    Stack(unsigned Size);
    Stack(const Stack &other);
    ~Stack();

    Stack& operator =(const Stack &other);

    void Push(char Value);
    void Pop();

    int Top();
    int Head();

    bool IsEmpty();
    bool IsFull();

    void PrintFront();
    void PrintStack();
};
