#pragma once
#include <iostream>
#include <string>
#include "stack.h"

using namespace std;

string RPN(const string &expr);
int Precedence(const char &c, Stack &stk, double &res);
