#pragma once

struct Node
{
    int Data;
    Node *Left;
    Node *Right;
    int Height;
};

class AVL
{
    Node *root;

    void MakeEmpty(Node *t);

    Node* Insert(int x, Node *t);
    Node* Remove(int x, Node *t);

    Node* SingleRightRotate(Node* &t);
    Node* SingleLeftRotate(Node* &t);

    Node* DoubleLeftRotate(Node* &t);
    Node* DoubleRightRotate(Node* &t);

    Node* FindMin(Node *t);
    Node* FindMax(Node *t);

    int height(Node *t);
    int GetBalance(Node *t);
    void InOrder(Node *t);

public:
    AVL();
    ~AVL();
    void Insert(int x);
    void Remove(int x);
    void Display();
};
