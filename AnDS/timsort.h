#pragma once

void InsertionSort(int *Arr, int left, int right);

//void MergeSort(int *Arr, int left, int right);

void Merge(int *Arr, int left, int mid, int right);

void TimSort(int *Arr, int size);

void PrintArr(int *Arr, int size);

const int RUN = 32;
