#include <iostream>
#include "stack.h"


//Stack :: Stack()
//{
//    Stack(5);
//}

Stack :: Stack(unsigned Size)
{
    std :: cout << this <<" is being constructed" << std :: endl;
    this -> Capacity = Size;
    this -> CurrentSize = 0;
    this -> arr = new char[this -> Capacity];
}


Stack :: Stack(const Stack &other)
{
    std :: cout << "Copy constructor is called " << this << std :: endl;

    this -> Capacity = other.Capacity;

    this -> arr = new char[other.Capacity];

    for (unsigned i = 0; i < other.Capacity; ++i)
        this -> arr[i] = other.arr[i];
}


Stack :: ~Stack()
{
    std :: cout << this << " is deconstructed" << std :: endl;
    delete[] arr;
}


Stack& Stack :: operator =(const Stack &other)
{
    this -> Capacity = other.Capacity;

    if (this -> arr != nullptr)
    {
        delete[] this -> arr;
    }

    this -> arr = new char[other.Capacity];

    for (unsigned i = 0; i < other.Capacity; ++i)
        this -> arr[i] = other.arr[i];

    return *this;
}


void Stack :: Push(char Value)
{
    if (IsFull())
        std :: cout << "STACK OVERFLOW" << std :: endl;
    else
    {
        this -> arr[this -> CurrentSize] = Value;
        this -> CurrentSize++;
    }
}


void Stack :: Pop()
{
    if (IsEmpty())
        std :: cout << "STACK IS EMPTY" << std :: endl;
    else
        this -> CurrentSize--;
}


bool Stack :: IsEmpty()
{
    return this -> CurrentSize == 0;
}

bool Stack :: IsFull()
{
    return this -> Capacity == this -> CurrentSize;
}


void Stack :: PrintFront()
{
    if(IsEmpty())
    {
        std :: cout << "STACK IS EMPTY" << std :: endl;
    }
    else {
        std :: cout << this -> arr[this -> CurrentSize - 1] << std :: endl;
    }
}


void Stack :: PrintStack()
{
    if (IsEmpty())
        std :: cout << "STACK IS EMPTY" << std :: endl;
    else
    {
        for (unsigned i = 0; i < this -> CurrentSize; ++i)
            std :: cout << this -> arr[i] << '\t';
        std :: cout << std :: endl;
    }
}


int Stack :: Top()
{
    return this -> arr[this -> CurrentSize - 1];
}


int Stack :: Head()
{
    return this -> arr[0];
}
