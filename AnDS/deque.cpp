#include <iostream>
#include "deque.h"


Deque :: Deque()
{
    this -> Head = nullptr;
    this -> Tail = nullptr;
}


void Deque ::PushBack(int Value)
{
    Node *tmp = new Node;
    tmp -> Next = nullptr;
    tmp -> Data = Value;

    if (this -> Head != nullptr)
    {
        tmp -> Prev = this -> Tail;
        this -> Tail -> Next = tmp;
        this -> Tail = tmp;
    }
    else
    {
        tmp -> Prev = nullptr;
        this -> Head = this ->Tail = tmp;
    }
}

void Deque :: PushFront(int Value)
{
    Node *tmp = new Node;
    tmp -> Prev = nullptr;
    tmp -> Data = Value;

    if (this -> Head != nullptr)
    {
        this -> Head -> Prev = tmp;
        tmp -> Next = this -> Head;
        this -> Head = tmp;
    }
    else
    {
        tmp -> Next = nullptr;
        this -> Head = this ->Tail = tmp;
    }
}


void Deque :: PopBack()
{
    Node *tmp = this -> Tail;
    this -> Tail = this -> Tail -> Prev;
    Tail -> Next = nullptr;
    delete tmp;
}

void Deque :: PopFront()
{
    Node *tmp = this -> Head;
    this -> Head = this -> Head -> Next;
    Head -> Prev = nullptr;
    delete tmp;
}


void Deque :: Show()
{
    Node *tmp = this -> Head;

    while (true)
    {
        std :: cout << tmp -> Data << '\t';
        if (tmp -> Next == nullptr)
            break;
        tmp = tmp -> Next;
    }
    std :: cout << std :: endl;
}


Deque :: ~Deque()
{
    while (this -> Head != nullptr)
        PopBack();
}
