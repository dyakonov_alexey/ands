TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    main.cpp \
    stack.cpp \
    deque.cpp \
    rpn.cpp \
    timsort.cpp \
    avltree.cpp

HEADERS += \
    stack.h \
    deque.h \
    rpn.h \
    timsort.h \
    avltree.h
