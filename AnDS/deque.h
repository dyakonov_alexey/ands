#pragma once

struct Node {
    int Data;
    Node *Next;
    Node *Prev;
};

class Deque {
    Node *Head;
    Node *Tail;

public:
    Deque();
    ~Deque();

    void PushBack(int Value);
    void PopBack();

    void PushFront(int Value);
    void PopFront();

    void Show();
};
