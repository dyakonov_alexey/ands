#include <iostream>
#include "timsort.h"

void InsertionSort(int *Arr, int left, int right)
{
    for (int i = left + 1; i <= right; i++)
    {
        int temp = Arr[i];
        int j = i - 1;
        while (Arr[j] > temp && j >= left)
        {
            Arr[j+1] = Arr[j];
            j--;
        }
        Arr[j+1] = temp;
    }
}


//void MergeSort(int *Arr, int left, int right)
//{
//    int mid;

//    if (left < right)
//    {
//        mid = (left + right) / 2;
//        MergeSort(Arr, left, mid);
//        MergeSort(Arr, mid + 1, right);
//        Merge(Arr, left, mid, right);
//    }
//}


void Merge(int *Arr, int left, int mid, int right)
{
    int len1 = mid - left + 1, len2 = right - mid;

    int L[len1], R[len2];

    for (int i = 0; i < len1; i++)
        L[i] = Arr[left + i];

    for (int i = 0; i < len2; i++)
        R[i] = Arr[mid + 1 + i];

    int i = 0;
    int j = 0;
    int k = left;


    while (i < len1 && j < len2)
    {
        if (L[i] <= R[j])
        {
            Arr[k] = L[i];
            i++;
        }
        else
        {
            Arr[k] = R[j];
            j++;
        }
        k++;
    }

    while (i < len1)
    {
        Arr[k] = R[i];
        k++;
        i++;
    }

    while (j < len2)
    {
        Arr[k] = R[j];
        k++;
        j++;
    }

}

void TimSort(int *Arr, int size)
{
    for (int i = 0; i < size; i+=RUN)
        InsertionSort(Arr, i, std :: min((i+31), (size-1)));

    for (int sizeRUN = RUN; sizeRUN < size; sizeRUN = 2*sizeRUN)
    {
        for (int left = 0; left < size; left += 2*sizeRUN)
        {
            int mid = left + sizeRUN - 1;
            int right = std :: min((left + 2*sizeRUN - 1), (size-1));

            Merge(Arr, left, mid, right);
        }
    }
}


void PrintArr(int *Arr, int size)
{
    for (int i = 0; i < size; ++i)
        std :: cout << Arr[i] << ' ';
    std :: cout << std :: endl;
}
